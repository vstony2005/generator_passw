unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Edit, FMX.EditBox, FMX.NumberBox, FMX.Layouts, FMX.Controls.Presentation,
  FMX.Objects, FMX.SpinBox;

type
  TForm1 = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    lytHeader: TLayout;
    Label1: TLabel;
    btnGo: TButton;
    GroupBox3: TGroupBox;
    rbB8: TRadioButton;
    rbB10: TRadioButton;
    rbB16: TRadioButton;
    chkNumbers: TCheckBox;
    lytFooter: TLayout;
    lblPassword: TLabel;
    btnCopy: TButton;
    chkLower: TCheckBox;
    chkUpper: TCheckBox;
    chkCaracters: TCheckBox;
    chkAccents: TCheckBox;
    chkPunctuation: TCheckBox;
    edtChars: TEdit;
    edtAccents: TEdit;
    edtPunctuation: TEdit;
    Line1: TLine;
    SpinBox1: TSpinBox;
    procedure btnCopyClick(Sender: TObject);
    procedure btnGoClick(Sender: TObject);
    procedure chkClick(Sender: TObject);
    procedure chkNumbersClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FList: TStringList;
    function GetCaracters: TStrings;
  public
  end;

const
  PWD_EMPTY: string = '-';

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  FMX.Platform, FMX.Clipboard;

procedure TForm1.FormCreate(Sender: TObject);
begin
  FList := TStringList.Create;
  lblPassword.Text := PWD_EMPTY;

  chkNumbers.IsChecked := True;
  chkUpper.IsChecked := True;
  chkLower.IsChecked := True;
  chkCaracters.IsChecked := True;
  chkAccents.IsChecked := True;
  chkPunctuation.IsChecked := True;

  rbB10.IsChecked := True;

  edtChars.Text := '%&+/<=>?@[\\]^_`{|}~$#*�';
  edtAccents.Text := '��������������������������������ٜ�';
  edtPunctuation.Text := ',-?.!\"''();��:;';
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  FList.Free;
end;

procedure TForm1.chkClick(Sender: TObject);
var
  chk: TCheckBox;
  comp: TComponent;
  i: Integer;
begin
  if (Sender is TCheckBox) then
  begin
    chk := Sender as TCheckBox;
    for comp in chk.Children do
      if (comp is TEdit) then
        (comp as TEdit).Enabled := not chk.IsChecked;
  end;
end;

procedure TForm1.chkNumbersClick(Sender: TObject);
begin
  GroupBox3.Enabled := not chkNumbers.IsChecked;
end;

procedure TForm1.btnGoClick(Sender: TObject);
var
  i: Integer;
  str: string;
begin
  GetCaracters;
  str := '';

  if (FList.Count = 0) then
  begin
    lblPassword.Text := PWD_EMPTY;
    Exit;
  end;

  for i := 0 to Trunc(SpinBox1.Value) - 1 do
    str := str + FList[Random(FList.Count)];

  lblPassword.Text := str;
end;

function TForm1.GetCaracters: TStrings;

  procedure AddChars(chk: TCheckBox; edt: TEdit);
  var
    i: Integer;
  begin
    if (chk.IsChecked) then
      for i := 0 to edt.Text.Length - 1 do
        FList.Add(edt.Text.Chars[i]);
  end;

var
  c: Char;
  i, base: Integer;
begin
  FList.Clear;

  if (chkLower.IsChecked) then
    for c := 'a' to 'z' do
      FList.Add(c);

  if (chkUpper.IsChecked) then
    for c := 'A' to 'Z' do
      FList.Add(c);

  AddChars(chkCaracters, edtChars);
  AddChars(chkAccents, edtAccents);
  AddChars(chkPunctuation, edtPunctuation);

  if (chkNumbers.IsChecked) then
  begin
    if (rbB16.IsChecked) then
      for i := 0 to 15 do
        FList.Add(IntToHex(i, 1))
    else
    begin
      if (rbB8.IsChecked) then
        base := 8
      else
        base := 10;

      for i := 0 to base - 1 do
        FList.Add(i.ToString);
    end;
  end;
end;

procedure TForm1.btnCopyClick(Sender: TObject);
var
  clpBoard: IFMXClipboardService;
begin
  if (lblPassword.Text = PWD_EMPTY) then
    Exit;

  if (TPlatformServices.Current.SupportsPlatformService(IFMXClipboardService,
    clpBoard)) then
    clpBoard.SetClipboard(lblPassword.Text);
end;

end.
